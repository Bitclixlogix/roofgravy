export const SERVER_URL = "https://gravy-api-ror.herokuapp.com/";
export const LOGIN      = "api/v1/users/login";
export const USERS      = "api/v1/users";
export const END_POINTS = {
  APPOINTMENTS: 'api/v1/calendars',
  JOBS: 'api/v1/jobs',
  LEADS: 'api/v1/leads',
  MESSAGES: 'api/v1/messages',
  PROFILE: 'api/v1/users/update_my_profile',
  TASKS: 'api/v1/tasks',
  TEMPLATES: 'api/v1/templates',
  USERS: "api/v1/users",
  FORGOT_PASSWORD:'api/v1/users/forgot_password',
  UPDATE_CURRENT_LOCATION:'api/v1/users/update_current_company_location'
}
